# lemPORT Web Service #

This project contains a little web service to perform lemmatization of 
Portuguese text. Tokenization and Part-of-Speech tagging are done with
[OpenNLP tools and models](https://opennlp.apache.org/); lemmatization is done using [LemPORT](https://github.com/rikarudo/LemPORT).

### Dependencies ###

* Java8
* Maven

### Get the code and compile ###

```bash
git clone https://apinhole@bitbucket.org/apinhole/lemport-service.git
cd lemport-service
mvn package
```
### Usage ###

Launch the server like so:
```bash
java -jar target/lemPORT-service-0.0.1-SNAPSHOT.jar --server.port=<port>
```

Then just make a POST request; for instance:
```bash
curl -X POST -H 'Content-Type: text/plain' -H 'Accept: text/csv' -d \
'A cidade de San Sebastián é a casa do Real Sociedad de Fútbol,
um dos principais clubes do futebol espanhol, a equipe manda seus
jogos no Estádio Anoeta, com capacidade para 32 mil pessoas.' \
http://localhost:<port>/lemmatize
```

You can access a UI of the web service at `http://localhost:<port>/swagger-ui.html`.

The response of the server is a CSV file, where each line represents a token 
of the input text: `wordform,part-of-speech,lemma`.
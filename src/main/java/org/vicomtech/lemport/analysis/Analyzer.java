package org.vicomtech.lemport.analysis;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.parsers.ParserConfigurationException;

import org.vicomtech.lemport.analysis.lemport.dictionary.DictionaryLoadException;
import org.vicomtech.lemport.analysis.lemport.lemma.LemmatizeException;
import org.vicomtech.lemport.analysis.lemport.lemma.Lemmatizer;
import org.vicomtech.lemport.analysis.lemport.rank.WordRankingLoadException;
import org.xml.sax.SAXException;

import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

public class Analyzer {
	
	private static final Logger logger = LoggerFactory.getLogger(Analyzer.class);
	
	public static String pathToTokenizerModel;
	public static String pathToPosTaggerModel;
	
	private Lemmatizer lemmatizer;
	private POSTaggerME posTagger;
	private Tokenizer tokenizer;

	public Analyzer() throws 
		IOException, 
		NumberFormatException, 
		ParserConfigurationException, 
		SAXException, 
		DictionaryLoadException, 
		WordRankingLoadException 
	{	
				
		logger.info("Making tokenizer");
		InputStream tokModelIn = new FileInputStream(pathToTokenizerModel);
		try {
			TokenizerModel tokModel = new TokenizerModel(tokModelIn);
			tokenizer = new TokenizerME(tokModel);
		} catch (IOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			tokModelIn.close();
			System.exit(0);
		}
		tokModelIn.close();
		logger.info("Done");
		
		logger.info("Making PoS tagger");
		InputStream posModelIn = new FileInputStream(pathToPosTaggerModel);
		try {
			POSModel posModel = new POSModel(posModelIn);
			posTagger = new POSTaggerME(posModel);
		} catch (IOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			posModelIn.close();
			System.exit(0);
		}
		posModelIn.close();
		logger.info("Done");
		
		logger.info("Making lemmatizer");
		lemmatizer = new Lemmatizer();
		logger.info("Done");
		
	}
	
	public String[] tokenize(String sentence){
		return tokenizer.tokenize(sentence);
	}
	
	public String[] postag(String[] tokens){
		return posTagger.tag(tokens);
	}
	
	public String[] lemmatize(String[] tokens, String[] tags){
		try {
			return lemmatizer.lemmatize(tokens, tags);
		} catch (LemmatizeException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	public String[][] full(String text){		
		String[] tokens = tokenize(text);
		String[] tags = postag(tokens);
		String[] lemmas = lemmatize(tokens, tags);
		if (lemmas != null){
			String[][] result = new String[tokens.length][3];
			for (int i = 0; i < tokens.length; i++){
				result[i][0] = tokens[i];
				result[i][1] = tags[i];
				result[i][2] = lemmas[i];
			}
			return result;
		}
		return null;
	}

}

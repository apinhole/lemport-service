package org.vicomtech.lemport.service;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import javax.xml.parsers.ParserConfigurationException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.vicomtech.lemport.analysis.Analyzer;
import org.vicomtech.lemport.analysis.lemport.dictionary.DictionaryLoadException;
import org.vicomtech.lemport.analysis.lemport.rank.WordRankingLoadException;
import org.xml.sax.SAXException;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class) 
@EnableScheduling
@EnableSwagger2
public class Application {
		
	public static void main(String[] args) throws 
		ClassNotFoundException, 
		FileNotFoundException, 
		SQLException, 
		IOException, 
		NumberFormatException, 
		ParserConfigurationException, 
		SAXException, 
		DictionaryLoadException, 
		WordRankingLoadException 
	{	
		setModelPaths();
		Controllers.analyzer = new Analyzer();
		SpringApplication.run(Application.class, args);		
	}
	
	private static void setModelPaths() throws IOException{
		String userDir = System.getProperty("user.dir");
		File modelsFolder = new File(userDir + File.separator + "models");
		if (!modelsFolder.exists() || modelsFolder.list().length == 0) {
			throw new IOException("Models directory not found");
		}
		File tokenizerModel = new File(userDir + File.separator + "models" + File.separator + "pt-token.bin");
		if (!tokenizerModel.exists()) {
			throw new IOException("Tokenizer model not found");
		}
		Analyzer.pathToTokenizerModel = tokenizerModel.getAbsolutePath();
		File posTagerModel = new File(userDir + File.separator + "models" + File.separator + "pt-pos-maxent.bin");
		if (!posTagerModel.exists()) {
			throw new IOException("PoS Tagger model not found");
		}
		Analyzer.pathToPosTaggerModel = posTagerModel.getAbsolutePath();
	}
	
	@Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.any())              
          .paths(PathSelectors.any())
          .build();                                           
    }

}
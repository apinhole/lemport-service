package org.vicomtech.lemport.service;

import org.assertj.core.util.Lists;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.vicomtech.lemport.analysis.Analyzer;

import io.swagger.annotations.ApiOperation;

@Controller
@CrossOrigin
public class Controllers {
	
	public static Analyzer analyzer;
	
	@ApiOperation(value="Lemmatize text")
	@RequestMapping(value="/lemmatize", method={RequestMethod.POST}, consumes="text/plain", produces="text/csv")
	public ResponseEntity<String> lemmatize(@RequestBody(required=true) String text){
		String[][] result = analyzer.full(text);
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Lists.newArrayList(MediaType.TEXT_PLAIN));
		headers.setContentType(new MediaType("text", "csv"));
		if (result != null){
			String resultString = serialize(result);
			return new ResponseEntity<String>(resultString, headers, HttpStatus.OK);
		} else {
			return new ResponseEntity<String>("", headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	private String serialize(String[][] result){
		StringBuilder sb = new StringBuilder();
		for (String[] triplet : result){
			String line = String.join(",", triplet);
			sb.append(line);
			sb.append('\n');
		}
		return sb.toString();
	}
		
}
